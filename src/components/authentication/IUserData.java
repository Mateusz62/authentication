package components.authentication;

import org.springframework.security.core.userdetails.User;

import java.util.Date;

/**
 * Created by Mateusz on 2014-12-12.
 */
public interface IUserData {
    boolean userExists(String userName);
    boolean emailUsed(String email);
    User getUserBySession(String sessionKey);
    User getUserByName(String userName);
    Date getSessionBeginDate(String sessionKey);
    boolean removeSession(String sessionKey);
    boolean addSession(String sessionKey, String userName);
    boolean addUser(String userName, String password, String email, String[] args);
}
