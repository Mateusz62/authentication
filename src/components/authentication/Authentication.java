package components.authentication;

import components.authentication.exceptions.UserAlreadyLoggedInException;
import components.authentication.exceptions.UserDoesNotExistsException;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;


/**
 * Created by Mateusz on 2014-12-10.
 */
public class Authentication implements IAuthentication {
    private String sessionKey = "";

    private IUserData userDataConnection;
    private Long sessionTime = 60000l;

    /**
     * Constructor - dependency injection.
     * @param userDataConnection Object that implements interface IUserData.
     */
    public Authentication(IUserData userDataConnection) {
        this.userDataConnection = userDataConnection;
    }

    /**
     * Constructor - dependency injection.
     * @param userDataConnection Object that implements IUserData.
     * @param sessionTime Time after which user will be automatically logged out (in milliseconds, 60k is default).
     */
    public Authentication(IUserData userDataConnection, Long sessionTime) {
        this.userDataConnection = userDataConnection;
        this.sessionTime = sessionTime;
    }

    /**
     * Function that performs logging in.
     * @param userName Name of user to be logged in.
     * @param password Password of a user.
     * @return Returns true if succeed and false otherwise.
     * @throws UserDoesNotExistsException   Self explanatory.
     * @throws UserAlreadyLoggedInException Self explanatory.
     */
    @Override
    public boolean login(String userName, String password)
            throws UserDoesNotExistsException, UserAlreadyLoggedInException {

        // Already logged in.
        if(!sessionKey.equals(""))
            return false;

        if (userName == null || userName.equals("")) {
            return false; // Empty userName.
        }

        if (password == null || password.equals("")) {
            return false; // Empty password.
        }

        // Ok, we've got something.
        if (!userDataConnection.userExists(userName)) {
            throw new UserDoesNotExistsException(String.format("User %s doesn't exists.", userName)); // User does not exists. He's not registered.
        }

        // HASHING
        // User Exists, check password. TODO how to hash?
        String hashedPassword = String.valueOf(password.hashCode());

        User user = userDataConnection.getUserByName(userName);

        if (user.getPassword().equals(hashedPassword)) {
            // check if user is not already logged in.
            if (userDataConnection.getUserByName(userName).isEnabled()) {
                throw new UserAlreadyLoggedInException(String.format("User %s is already logged in.", userName)); // User is already logged in.
            }

            // We're in. Proceed.
            // Log in.
            // Key identifying given session and new session with name and time.
            // Retrieve new key from UUID static class.
            sessionKey = UUID.randomUUID().toString();

            // Add it.
            userDataConnection.addSession(sessionKey, userName);
        }

        return true;
    }

    /**
     * Function that registers user.
     * @param userName Name of a user.
     * @param password Password of a user.
     * @param email Email of a user.
     * @param args Rest of the arguments that are going to be saved in database.
     * @return Returns true if registration succeed, false otherwise.
     */
    @Override
    public boolean register(String userName, String password, String email, String[] args) {//OK

        // Some validations.
        if (email == null || email.equals("") || userName == null || userName.equals("")
                || password == null || password.equals(""))
            return false;

        // Is email used, or user already exists?
        if (userDataConnection.emailUsed(email) || userDataConnection.userExists(userName)) {
            return false;
        }

        // Not? Then register user.
        userDataConnection.addUser(userName, password, email, args);

        // Done.
        return true;
    }

    /**
     * Logout user.
     */
    @Override
    public boolean logout() {
        userDataConnection.removeSession(sessionKey);
        sessionKey = "";
        return true;
    }

    @Override
    public boolean isUserLoggedIn() {
        if(sessionKey == null || sessionKey.equals("")) {
            return false;
        }

        getRemainingSessionTime();


        User user = userDataConnection.getUserBySession(sessionKey);

        if(user != null && user.isEnabled())
            return true;
        return false;
    }

    /**
     * Gets user name from given session.
     * @return Returns user name if given session is active and <b>null</b> otherwise.
     */
    @Override
    public String getUserName() {
        if(!isUserLoggedIn())
            return null;

        User user = userDataConnection.getUserBySession(sessionKey);
        return user.getUsername();
    }

    public Collection<GrantedAuthority> getUserAuthorities() {
        if(!isUserLoggedIn())
            return null;

        User user = userDataConnection.getUserBySession(sessionKey);
        return user.getAuthorities();
    }

    public boolean isUserInRole(GrantedAuthority authority) {
        Collection<GrantedAuthority> authorities = getUserAuthorities();
        if(authorities.contains(authority))
            return true;
        return false;
    }

    /**
     * Gets remaining time of a session. Returns 0 if session expired.
     * @return Remaining time in milliseconds.
     */
    @Override
    public Long getRemainingSessionTime() {
        if(sessionKey == null || sessionKey.equals(""))
            return 0l;

        Date sessionBegin = userDataConnection.getSessionBeginDate(sessionKey);
        // TODO Probably exception should be thrown.
        if(sessionBegin == null)
            return 0l;

        Long remTime = sessionTime -  ((new Date()).getTime() - sessionBegin.getTime());

        if(remTime <= 0) {
            logout();
            return 0l;
        }

        return remTime;
    }
}
