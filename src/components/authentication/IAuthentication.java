package components.authentication;

import components.authentication.exceptions.UserAlreadyLoggedInException;
import components.authentication.exceptions.UserDoesNotExistsException;

/**
 * Created by Mateusz on 2014-12-10.
 */

public interface IAuthentication {
    boolean login(String userName, String password) throws UserDoesNotExistsException, UserAlreadyLoggedInException;
    boolean register(String userName, String password, String email, String[] args);
    boolean logout();
    boolean isUserLoggedIn();
    String getUserName();
    Long getRemainingSessionTime();
}
