package components.authentication.testimpl;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by Mateusz on 2014-12-16.
 */
public class RandomUser implements GrantedAuthority {
    @Override
    public String getAuthority() {
        return "user";
    }
}
